/****HEADERS****/
#include <stdio.h>
#include "op_bits.h"

/****FUNCIONES DE VERIFICACIÓN DE ERRORES EN PARÁMETROS****/

/* CHECKBIT()
 * Función encargada de recibir el número de puerto solicitado, y analizar si hubo un error en lo solicitado.
 * 
 * ERRORES: se solicito el puerto A ó B, pero el número de bit excede el máximo
 *          se solicito un puerto inexistente
 *          se solicito un número de bit que excede el máximo posible
 *          se solicito un número de bit negativo
 * 
 * Devuelve: el estado de lo solicitado (ERROR  ó  NO_ERROR) 
 */

static int checkbit(int port, int bit); 
/*CHECKMASK()
 * Función encargada de analizar si la máscara dada como parámetro es adecuada para la operación.
 * 
 * Recibe: puerto para la cual se quiere utilizar la máscara y la mascara misma.
 * 
 * ERRORES: se quiere usar el puerto A y B, y la máscara utilia mas de 8 bits
 *          No se dio el puerto A, B ó D
 *          La máscara excede el máximo (16 bits)
 * 
 * Devuelve: validez de parámetros: ERROR ó NO_ERROR
 
 */
static int checkmask(int port, int mask); 

/*FUNCIONES AUXILIARES*/

/*GETMASK()
 * Función encargada de generar una máscara que tenga un 1 y luego 0s, en el bit solicitado y de acuerdo al puerto en el que se está trabajando.
 * 
 * Recibe: puerto en el que se está trabajando y el bit solicitado.
 *
 * Devuelve: máscara con 1 en el bit solicitado y el resto 0s.
 */
static int getmask(int port, int bit);

/****PUERTO****/
static puerto_t puerto;


/* CHECKBIT()
 * Función encargada de recibir el número de puerto solicitado, y analizar si hubo un error en lo solicitado.
 * 
 * ERRORES: se solicito el puerto A ó B, pero el número de bit excede el máximo
 *          se solicito un puerto inexistente
 *          se solicito un número de bit que excede el máximo posible
 *          se solicito un número de bit negativo
 * 
 * Devuelve: el estado de lo solicitado (ERROR  ó  NO_ERROR) 
 */
static int checkbit(int port, int bit)
{
    int estado = NO_ERROR;
    
    if(((port==A || port==B) && bit>MAXBIT_AB) || (port!=A && port!=B && port!=D) || bit>MAXBIT || bit<0)
    {
        estado = ERROR; // si ocurrió alguno error, cambiamos estado
    }
    return estado;//si estado no ccambio, entonces no hubo error, de lo contrario si ocurrió
    
    
}

/*CHECKMASK()
 * Función encargada de analizar si la máscara dada como parámetro es adecuada para la operación.
 * 
 * Recibe: puerto para la cual se quiere utilizar la máscara y la mascara misma.
 * 
 * ERRORES: se quiere usar el puerto A y B, y la máscara utilia mas de 8 bits
 *          No se dio el puerto A, B ó D
 *          La máscara excede el máximo (16 bits)
 * 
 * Devuelve: validez de parámetros: ERROR ó NO_ERROR
 
 */
static int checkmask(int port, int mask)
{
    int estado = NO_ERROR;
    
    if(((port==A || (port==B)) && mask>MAXMASK_AB) || (port!=A && port!=B && port!=D) || mask>MAXMASK)
    {
        estado = ERROR;//si ocurrión un error, cambiamos el estado
    }
    return estado;
}


/*GETMASK()
 * Función encargada de generar una máscara que tenga un 1 y luego 0s, en el bit solicitado y de acuerdo al puerto en el que se está trabajando.
 * 
 * Recibe: puerto en el que se está trabajando y el bit solicitado.
 *
 * Devuelve: máscara con 1 en el bit solicitado y el resto 0s.
 */

static int getmask(int port, int bit)
{
    int mask;
    
    if(port==B) //si es el puerto B estamos trabajando en el LSB, nos corremos esa cant a la der.
    {
        mask = 1<<bit;
    }
    else if(port==A) //si es el puerto A, estamos trabajando en el MSB, hace falta corrernos 8 bits a la der.
    {
        mask= 1<<(bit+8);
    }
    else //si estamos en registro D, el número de bit corresponde a desplazarse esa cant
    {
        mask=1<<bit;
    }

    return mask;
}

/*BITSET()
 *   Función encargada de recibir un puerto, y dejar el estado de ese bit en 1.
 * 
 *   Recibe:  puerto a tocar (A, B ó D) y el número de bit a setear.
 *   
 *   Devuelve: el estado de la operación (ERROR ó NO_ERROR)
 */
int bitSet( int port, int bit)
{
    int estado = checkbit(port, bit); //verificamos que el puerto y el número de bit solicitado corresponda
    int mask;
    
    if(estado == ERROR)
    {
        printf("ERROR EN LOS PARAMETROS EN BITSET()\n");
    }
    else
    {
        mask=getmask(port,bit);//en base al bit solicitado, creamos una máscara que solo tenga un 1 en ese bit
        
        puerto.reg_d |= mask;//hacemos un OR bitwise con el puerto para setearlo
    }
    
    return estado;//devolvemos el estado de la operación
}


/* BITCLEAR()
 * 
 * Función encargada de poner en 0 un bit solicitado.
 * 
 * Recibe: puerto solicitado (A, B ó D) y el número de bit solicitado.
 * 
 * Devuelve: estado de operación (ERROR ó NO_ERROR)
 */
int bitClr(int port, int bit)
{
    int estado = checkbit(port, bit);
    int mask;
    
    if(estado == ERROR)
    {
        printf("ERROR EN LOS PARAMETROS EN BITCLR()\n");
    }
    else
    {
        mask=~getmask(port,bit); //generamos una máscara que tenga 0 en el bit solicitado y 1s en el resto
        puerto.reg_d &= mask;//hacemos un AND bitwise con la máscara creada 
    }
    return estado; //devolvemos el estado de la operación
}


/*BITGET()
 * Función encargada de devolver el estado de un bit en  particular.
 * 
 * Recibe: puerto (A, B ó D) y el número de bit a analizar estado.
 * 
 * Devuelve: estado del bit solicitado o ERROR.
 */
int bitGet(int port, int bit)
{
    int estado = checkbit(port,bit); //verificamos el estado de los parámetros pasados
    int mask;
    
    if(estado == ERROR)
    {
        printf("ERROR EN LOS PARAMETROS EN BITGET()\n");
    }
    else //si los parámetros son correctos...
    {
        mask = getmask(port,bit);//creamos una máscara que tenga 1 en el bit solicitado, y 0 en el resto
        estado = ((puerto.reg_d & mask) ? HIGH : LOW); // si al hacer un AND con la máscara el resultado es 1, el bit está HIGH sino esta LOW
    }
    
    return estado;//devolvemos el estado de la operación
}


/*BITTOGGLE()
 * Función encargada de cambiar negar el estado de un bit solicitado (si está en 1 pasa a 0 y viceversa)
 * 
 * Recibe: puerto a modificar (A, B ó D) y el número de bit a modificar.
 * 
 * Devuelve: estado de la operación (ERROR ó NO_ERROR)
 */

int bitToggle(int port, int bit)
{
    int estado = checkbit(port,bit); //verificamos el estado de los parámetros
    int mask;
    if(estado == ERROR) //si ocurrió un error
    {
        printf("ERROR EN LOS PARAMETROS EN BITTOGGLE()\n");
    }
    else //si no ocurrió ningun error
    {
        mask=getmask(port,bit); //creamos una máscara que tenga un 1 en el bit solicitado y 0 en el resto
        puerto.reg_d ^= mask;//hacemos una "negación controlada" con la XOR (niega solo los bits que tienen un 1 en el control), por lo que al hacerlo con la máscara niega solo el bit solicitado
    }
    
    return estado;//devolvemos el estado de la operación
}


/*MASKON()
 * 
 * Función encargada de setear los bits que estan en 1 de una máscara dada en un puerto.
 * 
 * Recibe: puerto (A, B ó D) y máscara a emplear en la operación.
 * 
 * Devuelve: estado de la operación (ERROR ó NO_ERROR). 
 */
int maskOn(int port, int mask)
{
    int estado = checkmask(port,mask);//verificamos la validez de los parámetros dados
    
    if(estado == ERROR)//si ocurrió un error...
    {
        printf("ERROR EN LOS PARAMETROS EN MASKON()\n"); 
    }
    else//si no ocurrió un error...
    {
        if(port==A)
        {
            mask <<= 8;// dado que el puerto A está en la parte alta del puerto, movemos 8 lugares a la der la máscara
        }
        puerto.reg_d |= mask;//hacemos un OR bitwise con la máscara dada 
    }

    return estado;//devolvemos el estado de la operación 
}

/*MASKOFF()
 * 
 * Función encargada de hacer clear a los bits que estan en 1 de una máscara dada en un puerto.
 * 
 * Recibe: puerto (A, B ó D) y máscara a emplear en la operación.
 * 
 * Devuelve: estado de la operación (ERROR ó NO_ERROR). 
 */
int maskOff(int port, int mask)
{
    int estado = checkmask(port,mask); //verificamos la valiz de los parámetros
    if(estado == ERROR)//si ocurrió un error
    {
        printf("ERROR EN LOS PARAMETROS EN MASKOFF()\n");
    }
    else
    {
        if(port==A)
        {
            mask<<=8;//si se solicitó el puerto A, este esta el el MSB, por lo que corremos 8 bits a la derecha la máscara dada
        }
        puerto.reg_d &= ~mask; //hacemos un AND bitwise con  el CA1 de la máscara dada (de forma de que haya 0s en donde queremos mod. y 1s donde no)
    }
    return estado; //devolvemos el estado de la operación
}

/*MASKTOGGLE()
 * 
 * Función encargada de negar el estado de los bits solicitados en una máscara (hay que modificar aquellos que estan en 1 y dejar los que están en 0).
 * 
 * Recibe: puerto a modificar y máscara a utilizar.
 * 
 * Devuelve: estado de la operación.
 */
int maskToggle(int port, int mask)
{
    int estado = checkmask(port,mask); //analizamos la validez de los parámetros pasados.
    
    if(estado == ERROR)//si ocurrió un error...
    {
        printf("ERROR EN LOS PARAMETROS EN MASKTOGGLE()\n");
    }
    else //si no ocurrió un error...
    {
        if(port==A) // si se solicita el puerto A, hay que correr la máscara 8 lugares a la der., pues A está en el MSB
        {
            mask <<= 8;
        }
        puerto.reg_d ^= mask; //hacemos una "negación controlada" por medio de una XOR (niega aquellos bits que estan en 1 en la máscara - el control - y deja sin negar el resto)
    }
    return estado;
}