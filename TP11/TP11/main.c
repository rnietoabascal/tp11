/*HEADERS*/
/*librerias standard*/
#include <stdio.h>
#include <stdlib.h>

/*librerias de allegro*/
#include <allegro5/allegro_color.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_font.h>
#include "allegro5/allegro_ttf.h"
#include <allegro5/allegro_audio.h> 
#include <allegro5/allegro_acodec.h> //Extensiones con acodec .wav, .flac, .ogg, .it, .mod, .s3m, .xm. 

/*librerias de manejo de bits*/
#include "op_bits.h"

/*TAMAÑO DE LA PANTALLA*/
#define SCREEN_W  1000
#define SCREEN_H  700

/*MACROS DE COORDENAS DE LEDs*/
#define COORD_Xi 80
#define COORD_X_STEP 120
#define COORD_Y SCREEN_H/2
#define LED_RAD 40
#define MARCO_RAD LED_RAD + LED_RAD/10
#define COORD_Y_STEP_TXT 50

/*TIEMPO DE PARPADEO*/
#define FPS    5  


/*MACROS DE LAS TECLAS*/
#define	NEG_STATE ALLEGRO_KEY_T //estado opuesto
#define	CLEAR ALLEGRO_KEY_C //interruptor apagado
#define	SET ALLEGRO_KEY_S //interruptor encendido
#define	QUIT ALLEGRO_KEY_Q //cierre del programa
#define PARPADEA ALLEGRO_KEY_B //parpadeo de los LEDs encendidos

/*POSICIÓN DEL LED*/
#define LED_POS A

int main(int argc, char** argv) 
{
    /*PUNTEROS DE LOS ELEMENTOS*/
    ALLEGRO_DISPLAY *display = NULL; //puntero al display que vamos a usar
    ALLEGRO_EVENT_QUEUE *event_queue = NULL; //puntero a la cola de eventos
    ALLEGRO_TIMER *timer = NULL; //puntero a timer
    ALLEGRO_FONT *font = NULL; //puntero a fuente
    ALLEGRO_SAMPLE *sample1 = NULL; //punteros a audio
    ALLEGRO_SAMPLE *sample2 = NULL;
    ALLEGRO_SAMPLE *sample3 = NULL;
    
    /*VARIABLES DE ESTADO*/
    bool parpadea = true;
    bool modoB = false;
    bool redraw = true;
    bool display_close = false;
    
    /*MENSAJES*/
    const char* msj[]={"0", "1", "2", "3", "4", "5", "6", "7"};
    
    al_set_new_window_title("Emulador de LED en puerto"); //le ponemos nombre a la ventana
    
    /*INICIALIZACIÓN DEL SISTEMA*/
    if (!al_init()) //inicializamos Allegro 
    {
        fprintf(stderr, "Error al inicializar alegro!\n");
        return EXIT_FAILURE;
    }

    if (!al_install_keyboard()) //inicializamos el teclado
    {
        fprintf(stderr, "Error al inicializar el teclado!\n");
        return EXIT_FAILURE;
    }


     timer = al_create_timer(1.0 / FPS); //crea el timer pero NO empieza a correr
     
    if (!timer) //verificamos que se haya creado el timer
    {
        fprintf(stderr, "Fallo al crear el timer!\n");
        return EXIT_FAILURE;
    }

    event_queue = al_create_event_queue(); //creamos la cola de eventos
    
    if (!event_queue) //si la cola de eventos no se creo... 
    {
        fprintf(stderr, "Falla al crear la cola de eventos!\n");
        al_destroy_timer(timer);
        return EXIT_FAILURE;
    }

    display = al_create_display(SCREEN_W, SCREEN_H);//creamos un display de 1000 x 700
    
    if (!display)//si el display no se creo... 
    {
        fprintf(stderr, "Falla al crear el display!\n");
        al_destroy_event_queue(event_queue);
        al_destroy_timer(timer);
        return EXIT_FAILURE;
    }
    
    if (!al_install_mouse())
    {
        fprintf(stderr, "Falla al inicializar el mouse!\n");
        return EXIT_FAILURE;
    }
    
    if(!al_init_font_addon() && !al_init_ttf_addon())
    {
        fprintf(stderr, "Falla al inicializar fuente");
        return EXIT_FAILURE;
    }
    
    font = al_create_builtin_font(); //creamos fuente
  
    if(!font)
    {
        fprintf(stderr, "Falla al crear la fuente");
        al_destroy_font(font);
        return EXIT_FAILURE; 
    }
    
    if (!al_install_audio()) 
    {
        fprintf(stderr, "Falla al inicializar audio!\n");
        return EXIT_FAILURE;
    }
    
    /*INICIAIZACIÓN DE AUDIO*/
    if(!al_init_acodec_addon()) 
    {
        fprintf(stderr, "Falla al inicializar audio codecs!\n");
        return EXIT_FAILURE;
    }

    if(!al_reserve_samples(3)) 
    {
        fprintf(stderr, "Falla al reservar muestras!\n");
        return EXIT_FAILURE;
    }

    sample1 = al_load_sample("musica.wav"); //cargamos el audio para el modo parpadeo

    if(!sample1) 
    {
        printf("No se cargo el audio 1!\n");
        return EXIT_FAILURE;
    }
    
    sample2 = al_load_sample("mixkit-arcade-video-game-pop-2887.wav"); //cargamos el audio para el click

    if(!sample2) 
    {
        printf("No se cargo el audio 2!\n");
        return EXIT_FAILURE;
    }
    
    sample3 = al_load_sample("mixkit-funny-fail-low-tone-2876.wav"); //cargamos el audio para el salir
    if(!sample3) 
    {
        printf("No se cargo el audio 2!\n");
        return EXIT_FAILURE;
    }
    
    al_init_primitives_addon(); //inicializamos las primitivas (circulos, elipses, etc
    
    /*EVENTOS*/
    //vamos a analizar eventos de display, teclado, timer y mouse
    al_register_event_source(event_queue, al_get_keyboard_event_source());
    al_register_event_source(event_queue, al_get_display_event_source(display));
    al_register_event_source(event_queue, al_get_timer_event_source(timer));
    al_register_event_source(event_queue, al_get_mouse_event_source());
    
    /*INICIALIZACIÓN DEL DISPLAY*/
    al_clear_to_color(al_map_rgb(0, 0, 0));
    al_flip_display();

    al_start_timer(timer); //comienza el timer
        
    while (!display_close) //si no se presiona el boton de cerrar ventana...
    {
        ALLEGRO_EVENT ev; //definimos la variable para eventos
        
        /*EVENTOS DE TIMER*/
        if (al_get_next_event(event_queue, &ev)) //Toma un evento de la cola, VER RETURN EN DOCUMENT.
        {
            if (ev.type == ALLEGRO_EVENT_TIMER && modoB)
            {
                //Si modo B esta apagado, la variable parpadea queda fija en 1
                    parpadea = parpadea? false : true;
                    redraw=true;        //este redraw hace que se dibuje TODO cada el tiempo del timer
    
            }
            /*EVENTOS DE TECLADO*/
            else if(ev.type == ALLEGRO_EVENT_KEY_DOWN)
            {
                switch(ev.keyboard.keycode)
                {
                    case NEG_STATE:
                        maskToggle(LED_POS,0xFF); //Reemplaza TODOS los leds por el estado opuesto
                        al_play_sample(sample2, 1.0, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        redraw=true;  
                        break;
                        
                    case CLEAR:   
                        maskOff(LED_POS, 0xFF);//Apaga TODOS los leds
                        redraw=true;
                        al_play_sample(sample2, 1.0, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        break;
                        
                    case SET:
                        maskOn(LED_POS, 0xFF);//Enciendo TODOS los leds
                        redraw=true;
                        al_play_sample(sample2, 1.0, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        break;
                        
                    case PARPADEA:
                        modoB = modoB? false : true;//Cada vez que ingrese una b, entra o sale del modo b
                        if(!modoB)
                        {
                            parpadea=true;
                        }
                        redraw=true;
                        al_stop_samples();
                        if(modoB)
                        {      
                            al_play_sample(sample1, 1.0, 0.0, 2.0, ALLEGRO_PLAYMODE_LOOP, NULL);
                        }
                        break;
                        
                    case QUIT:
                        display_close=true;
                        break;
                        
                    default:
                        if( ev.keyboard.keycode >= ALLEGRO_KEY_0 && ev.keyboard.keycode <= ALLEGRO_KEY_7)
                        {
                            bitSet(LED_POS, ev.keyboard.keycode - ALLEGRO_KEY_0); //seteamos el bit ingresado
                            redraw=true;
                            al_play_sample(sample2, 1.0, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL);
                        }
                        break;       
                }
            }
            /*EVENTOS DE MOUSE*/
            else if(ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN)
            {
                int i;

                for(i=0; i<=MAXBIT_AB; i++)
                {
                    if(ev.mouse.x <= COORD_Xi + i * COORD_X_STEP + LED_RAD && ev.mouse.x >= COORD_Xi + i * COORD_X_STEP - LED_RAD && ev.mouse.y >= COORD_Y - LED_RAD && ev.mouse.y <= COORD_Y + LED_RAD )
                    { //si presionamos el LED cambiamos el estado
                        if(!bitGet(LED_POS,MAXBIT_AB-i))
                        {
                            bitSet(LED_POS, MAXBIT_AB -i); //si estaba apagado, lo prendemos
                        }
                        else
                        {
                            bitClr(LED_POS, MAXBIT_AB -i); //si estaba prendido lo apagamos
                        }
                        al_play_sample(sample2, 1.0, 0.0, 2.0, ALLEGRO_PLAYMODE_ONCE, NULL); //sonido de presionado        
                    }
                }
                redraw=true;   
            }
            
            /*EVENTOS DE DISPLAY*/
            else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
            {
                display_close = true;
            }
        }
        
        /*CARGAMOS EL DISPLAY*/
        if (redraw)//si se redibuja los LEDs ...
        {
            int i;
            for(i=0; i<=MAXBIT_AB; i++)
            {
                al_draw_filled_circle(COORD_Xi + i * COORD_X_STEP, COORD_Y, MARCO_RAD, al_color_name("silver")) ;
                al_draw_filled_circle(COORD_Xi + i * COORD_X_STEP, COORD_Y, LED_RAD, (bitGet(LED_POS,MAXBIT_AB-i) && parpadea) ? al_color_name("red") : al_color_name("white")) ;
                /*Si parpadea esta en 1, solo dependera del bitget de cada bit, si parpadea esta en 0 se veran todos apagados sin importar su getbit,
                y a su vez, cuando sea necesario parpadea oscila entre 1 y 0 haciendo parpadear aquellos leds que esten encendidos*/
                al_draw_text(font, (bitGet(LED_POS,MAXBIT_AB-i) && parpadea) ? al_color_name("red") : al_color_name("white"), COORD_Xi + i * COORD_X_STEP,  COORD_Y + COORD_Y_STEP_TXT, 0, msj[MAXBIT_AB - i]);
            }
            al_flip_display(); //cargamos el buffer en el display
            redraw = false;
            
        }

    }
    
    al_stop_samples();//paramos de reproducir todos los sonidos
    al_play_sample(sample3, 1.0, 0.0, 0.75, ALLEGRO_PLAYMODE_ONCE, NULL);//reproducimos la música salida
    al_destroy_display(display); //destruimos el display
    al_rest(1.2);
    
    /*FINALIZAMOS DESTRUYENDO LOS OBJETOS CREADOS*/
    al_destroy_timer(timer);
    al_destroy_event_queue(event_queue);
    al_destroy_font(font);
    al_destroy_sample(sample1);
    al_destroy_sample(sample2);
    al_destroy_sample(sample3);
    al_uninstall_audio();

    return (EXIT_SUCCESS);
}

