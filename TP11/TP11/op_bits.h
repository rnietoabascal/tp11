#ifndef OP_BITS_H
#define OP_BITS_H

/****HEADERS****/
#include <stdint.h>

/****CONSTANTES****/

/*PLATAFORMA*/

#define LITTLE_ENDIAN

/*REGISTROS*/
enum registros {A,B,D};

/*MACROS DE CHECKBIT*/
#define MAXBIT 15 //máximo número de bit que se puede solicitar en 16 bits
#define MAXBIT_AB 7 //máximo número de bit que se puede solicitar en 8 bits

#define MAXMASK 0xFFFF//máscara más grande que se puede utilizar en 16 bits
#define MAXMASK_AB 0xFF//máscara más grande que  se puede utilizar en 8 bits

/*MACROS DE BITGET*/
#define HIGH 1
#define LOW 0

/****ERRORES****/
#define ERROR -1
#define NO_ERROR 0

/****ESTRUCTURAS****/

/*REGISTROS A Y B*/
typedef struct
{
#ifdef LITTLE_ENDIAN //si el plataforma es little endian, el registro B va primero en memoria
    uint8_t reg_b;
    uint8_t reg_a;
#endif

#ifdef BIG_ENDIAN //si la plataforma es big endian, el registro A va primero en memoria
    uint8_t reg_b;
    uint8_t reg_a;
#endif
} REG_AB_T;

/*PUERTO DE 16 BITS: D*/
typedef union
{
    uint16_t reg_d; //el registro D corresponde al acumlado de los registros A y B
    REG_AB_T reg_ab;
} puerto_t;

/****PROTOTIPOS****/
/*BITSET()
 *   Función encargada de recibir un puerto, y dejar el estado de ese bit en 1.
 * 
 *   Recibe:  puerto a tocar (A, B ó D) y el número de bit a setear.
 *   
 *   Devuelve: el estado de la operación (ERROR ó NO_ERROR)
 */
int bitSet(int port, int bit);

/* BITCLEAR()
 * 
 * Función encargada de poner en 0 un bit solicitado.
 * 
 * Recibe: puerto solicitado (A, B ó D) y el número de bit solicitado.
 * 
 * Devuelve: estado de operación (ERROR ó NO_ERROR)
 */
int bitClr(int port, int bit);

/*BITGET()
 * Función encargada de devolver el estado de un bit en  particular.
 * 
 * Recibe: puerto (A, B ó D) y el número de bit a analizar estado.
 * 
 * Devuelve: estado del bit solicitado o ERROR.
 */
int bitGet(int port, int bit);


/*BITTOGGLE()
 * Función encargada de cambiar negar el estado de un bit solicitado (si está en 1 pasa a 0 y viceversa)
 * 
 * Recibe: puerto a modificar (A, B ó D) y el número de bit a modificar.
 * 
 * Devuelve: estado de la operación (ERROR ó NO_ERROR)
 */
int bitToggle(int port, int bit);

/*MASKON()
 * 
 * Función encargada de setear los bits que estan en 1 de una máscara dada en un puerto.
 * 
 * Recibe: puerto (A, B ó D) y máscara a emplear en la operación.
 * 
 * Devuelve: estado de la operación (ERROR ó NO_ERROR). 
 */
int maskOn(int port, int mask);


/*MASKOFF()
 * 
 * Función encargada de hacer clear a los bits que estan en 1 de una máscara dada en un puerto.
 * 
 * Recibe: puerto (A, B ó D) y máscara a emplear en la operación.
 * 
 * Devuelve: estado de la operación (ERROR ó NO_ERROR). 
 */
int maskOff(int port, int mask);

/*MASKTOGGLE()
 * 
 * Función encargada de negar el estado de los bits solicitados en una máscara (hay que modificar aquellos que estan en 1 y dejar los que están en 0).
 * 
 * Recibe: puerto a modificar y máscara a utilizar.
 * 
 * Devuelve: estado de la operación.
 */
int maskToggle(int port, int mask);
#endif /* OP_BITS_H */

